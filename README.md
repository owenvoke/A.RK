# A.RK Repo

[![Join the chat at https://gitter.im/PXgamer/A.RK](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/PXgamer/A.RK?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) [![A.RK Wiki](https://img.shields.io/badge/A.RK-Wiki-red.svg)](https://gitlab.com/PXgamer/A.RK/wikis/home)

ARK (stylised as A.RK [or Amaze.RK]) is a movie site (currently under development).

*A.RK will not compile unless line 25 in Login.cs is commented out.*

I am currently using the OMDB API and the TV Maze API.
Please see the [Wiki](https://gitlab.com/PXgamer/A.RK/wikis/home) for more information.

This is just a project I am doing in my spare time and not really for any purpose other than learning more about C#.
